package cucumber.classes;

import java.util.Arrays;

public class User {

    //variables
    public  String[] users = {"Cesar","Sebastian","Jose","Igor", "user1", "user2", "user3"};
    private String password;
    private String username;

    public static String correctUsername(String username) {
        return "Rocio".equals(username) ? "Yes" : "No";
    }

    public boolean verification(String user) {
        boolean exist = true;
        if (Arrays.asList(this.users).contains(user)) {
            exist = true;
        } else {
            exist = false;
        }
        return exist;
    }

    public static String accessTo(String password) {
        return "Cocacola".equals(password) ? "Administration tools" : "Home";
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
