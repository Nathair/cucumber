package cucumber.classes;

public class Profile {
    public static String correctProfile(String profile) {
        return "admin".equals(profile) ? "You have access" : "You don't have access";
    }
}
