package cucumber.test;

import cucumber.classes.User;
import cucumber.classes.Profile;
import io.cucumber.core.exception.CucumberException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;

public class StepDefinitions {

    //Variables
    private final User objUser = new User();
    private String user;
    private String password;
    private String profile;
    private String correctUser;
    private String correctProfile;
    private String message = "";
    private String correctPassword;
    private String registeredUser;
    private String notRegisteredUser;
    private Boolean registered = false;

    //User
    @Given("User is {string}")
    public void user_is(String user) throws CucumberException {
        this.user = user;
    }

    @When("I ask whether user is allowed")
    public void i_ask_whether_user_is_allowed() throws CucumberException {
        this.correctUser = User.correctUsername(user);
    }

    @Then("user should be told {string}")
    public void user_should_be_told(String expectedAnswer) throws CucumberException {
        assertEquals(expectedAnswer, this.correctUser);
    }

    //Profile
    @Given("profile is {string}")
    public void profile_is(String profile) throws CucumberException {
        this.profile = profile;
    }

    @When("I ask whether Profile has permission")
    public void i_ask_whether_profile_has_permission() throws CucumberException {
        this.correctProfile = Profile.correctProfile(profile);
    }

    @Then("profile should return {string}")
    public void profile_should_return(String expectedAnswer) throws CucumberException {
        assertEquals(expectedAnswer, this.correctProfile);
    }

    //User Creation
    @Given("The user {string} now exists")
    public void newUser(String newUser){
        this.user = newUser;
    }

    @When("I create a new User in the system")
    public void newUserCreation() {
        boolean exist = objUser.verification(this.user);
        if(exist){
            message = "User Already Exist";
        }else{message ="User created"; }
    }

    @Then("Receive a message {string}")
    public void message(String expectedMessage){
        assertEquals(expectedMessage,message);
    }

    //Users access to component
    @Given("User password is {string}")
    public void password_is(String password) throws CucumberException {
        this.password = password;
    }

    @When("User types his password")
    public void user_types_his_password() throws CucumberException {
        this.correctPassword = User.accessTo(this.password);
    }

    @Then("User should access {string}")
    public void user_should_access_corresponding(String expectedAnswer) throws CucumberException {
        Assert.assertEquals(expectedAnswer, this.correctPassword);
    }

    //Existing user
    @Given("user is registered")
    public void user_is_registered() throws CucumberException {
        this.registeredUser = "user1";
    }

    @When("registered user access application")
    public void registered_user_access_application() throws CucumberException {
        for (int i = 0; i < this.objUser.users.length; i++) {
            if (this.registeredUser.equals(this.objUser.users[i])) {
                this.registered = true;
                break;
            }
        }
    }

    @Then("Login should verify correct {string}")
    public void login_should_verify_correct_password(String expected) throws CucumberException {
        if (registered) {
            this.registeredUser = "password";
        }
        Assert.assertEquals(expected, this.registeredUser);
    }

    //Not existent user
    @Given("user is not registered")
    public void user_is_not_registered() throws CucumberException {
        this.notRegisteredUser = "hiMark";
    }

    @When("Not registered user access application")
    public void not_registered_user_access_application() throws CucumberException {
        for (int i = 0; i < this.objUser.users.length; i++) {
            if (this.notRegisteredUser.equals(this.objUser.users[i])) {
                this.registered = false;
                break;
            }
        }
    }

    @Then("User should be asked for {string}")
    public void user_should_be_asked_for_registration(String expected) throws CucumberException {
        if (!registered) {
            this.notRegisteredUser = "registration";
        }
        Assert.assertEquals(expected, this.notRegisteredUser);
    }

    //User set password
    @When("I set my password as {string}")
    public void iSetMyPasswordAs(String password) {
        objUser.setPassword(password);
    }

    @Then("A message appears who says {string}")
    public void aMessageAppearsWhoSays(String message) {
        System.out.println(message+objUser.getPassword());
    }

    //Scenario Outline: User put a wrong password

    @When("I fill in password with {string}")
    public void i_fill_in_with(String fill) {
        password = fill;
    }

    @Then("I should see {string}")
    public void i_should_see(String alert) {
        String existentPassword = this.objUser.getPassword();
        if (existentPassword != password) {
            System.out.println(alert);
        }
    }
}
