Feature: Login permissions
  We need to check the users and profiles permissions in the system

  Scenario Outline: User is allowed
    Given User is "<user>"
    When I ask whether user is allowed
    Then user should be told "<answer>"

    Examples:
      | user        | answer |
      | Rocio       | Yes    |
      | someone     | No     |

  Scenario Outline: Profile has permission
    Given profile is "<profile>"
    When I ask whether Profile has permission
    Then profile should return "<answer>"

    Examples:
      | profile     | answer                |
      | admin       | You have access       |
      | user        | You don't have access |

  Scenario Outline: User should access to the corresponding component
    Given User password is "<password>"
    When User types his password
    Then User should access "<component>"

    Examples:
      | password      | component             |
      | Cocacola      | Administration tools  |
      | somethingElse | Home                  |

  Scenario: User exists in the system
    Given user is registered
    When registered user access application
    Then Login should verify correct "password"

  Scenario: User doesn't exists in the system
    Given user is not registered
    When Not registered user access application
    Then User should be asked for "registration"

  Scenario Outline: Create a new User
    Given The user "<user>" now exists
    When I create a new User in the system
    Then Receive a message "<expectedMessage>"

    Examples:
      | user        | expectedMessage     |
      | Rocio       | User created        |
      | Igor        | User Already Exist  |
      | Sebastian   | User Already Exist  |
      | John        | User created        |

  Scenario Outline: User set a password
    Given User is <username>
    When I set my password as "goodpassword"
    Then A message appears who says "Congratulations! Your password is "
    Examples:
      | username |
      | "Rocio"  |

  Scenario Outline: User put a wrong password
    Given User is <username>
    When I fill in password with <fill>
    Then I should see <alert>
    Examples:
      | username | fill          | alert                                   |
      | "Rocio"  | "badpassword" | "The password is incorrect. Try Again." |